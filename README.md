`discord-send-embed` is a Discord bot/HTTP API that takes HTTP requests and posts the info from them as rich messages in a Discord server.

I'm new to Rust and I made it quickly to cover one specific need of mine so it's a bit rough in some areas (error handling, all config is in a static file) - if (for some reason) you wanted to use it open an issue and I'll tidy it up a bit.

### deploying
`cargo build --release` spits out a binary that *should* just work wherever you drop it - I like to stick it somewhere in `/srv`. There's a systemd service file in `share/discord-send-embed.service` if that's your thing.

### config
All config for the bot happens in `config.toml` in the same directory as the binary.
```toml
[web]
port = 8080 # port for HTTP listener

[discord]
token = "<redacted>" # Discord bot API token - from discordapp.com/developers
prefix = ">" # prefix for commands to the chatbot

[[channel]] # have as many as these as you need
name = "test" # just for your own reference (though not optional)
channel_id = <redacted> # Discord channel ID (no quote marks)
key = "<redacted>" # secret key used for HTTP requests - make your own and put it here
```

### usage
The Discord bot responds to one command - `<prefix> ping`, which can be used to check it's still running

To send a message in Discord, send a POST request to the server (any route) with `Content-Type = application/json`
```json
{
	"key": "", // secret key
	"title": "", // Heading
	"url": "", // Link destination - cannot be left blank and `http[s]://` is required
	"body": "" // main text
}
```