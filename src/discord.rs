use serenity::client::{Client, EventHandler};
use serenity::command;
use serenity::framework::StandardFramework;
use serenity::model::id::ChannelId;

use crate::config;

struct Handler;
impl EventHandler for Handler {}

pub(crate) fn make_client(config: &config::Config) -> Client {
    let mut client = Client::new(&config.discord.token, Handler).expect("bad client build");

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.allow_whitespace(true).prefix(&config.discord.prefix))
            .cmd("ping", ping),
    );

    client
}

pub(crate) fn start_discord(mut client: Client) {
    println!("Client running!");

    // blocks - spin off into seperate thread??
    if let Err(why) = client.start() {
        println!("Error! {:?}", why);
    }
}

pub(crate) fn send_embed(channel_id: ChannelId, title: &str, url: &str, body: &str) {
    channel_id
        .send_message(|m| m.embed(|e| e.title(title).url(url).description(body)))
        .ok();
}

command!(ping(_context, message) {
    let _ = message.reply("Pong");
});
