use serde::Deserialize;
use std::fs;

#[derive(Deserialize, Debug)]
pub(crate) struct Config {
    pub discord: Discord,
    pub web: Web,
    pub channel: Vec<Channel>,
}

#[derive(Deserialize, Debug)]
pub(crate) struct Web {
    pub port: u16,
}

#[derive(Deserialize, Debug)]
pub(crate) struct Discord {
    pub token: String,
    pub prefix: String,
}

#[derive(Deserialize, Debug)]
pub(crate) struct Channel {
    pub name: String,
    pub channel_id: u64,
    pub key: String,
}

pub(crate) fn make_config() -> Config {
    let config_file =
        fs::read_to_string("config.toml").expect("Something went wrong reading the file");

    let config: Config = toml::from_str(&config_file).unwrap();

    config
}
