use tiny_http::Header;
use tiny_http::Method;
use tiny_http::Response;
use tiny_http::Server;

use serde::Deserialize;

use crate::config;
use crate::discord;

#[derive(Deserialize, Debug)]
struct Message {
    key: String,
    title: String,
    url: String,
    body: String,
}

pub(crate) fn make_server(config: &config::Config) -> Server {
    // may god have mercy on my soul
    let mut address = "0.0.0.0:".to_string();
    address.push_str(&config.web.port.to_string());

    Server::http(address).unwrap()
}

pub(crate) fn start_web(server: &Server, config: &config::Config) {
    // only one worker

    let ideal_header: Header =
        tiny_http::Header::from_bytes(&b"Content-Type"[..], &b"application/json"[..]).unwrap();

    println!("HTTP server running on http://{}/ !", server.server_addr());

    loop {
        let mut request = match server.recv() {
            Ok(rq) => rq,
            Err(e) => {
                println!("HTTP server error: {}", e);
                break;
            }
        };

        if request.method() != &Method::Post {
            let response = Response::from_string("405: Method Not Allowed");
            let response = response.with_status_code(405);
            let _ = request.respond(response);
            continue;
        }

        let mut content_is_json = false;

        // look at get_content_type
        for h in request.headers() {
            // sorry
            if h.field == ideal_header.field && h.value == ideal_header.value {
                content_is_json = true;
                continue;
            }
        }

        if !content_is_json {
            let response =
                Response::from_string("400: Bad Request (Content-Type is not application/json)");
            let response = response.with_status_code(400);
            let _ = request.respond(response);
            continue;
        }

        let mut raw_req_body = String::new();
        request
            .as_reader()
            .read_to_string(&mut raw_req_body)
            .unwrap();

        let req_struct_result: serde_json::Result<Message> = serde_json::from_str(&raw_req_body);

        let req_struct = match req_struct_result {
            Ok(message) => message,
            Err(_error) => {
                let response = Response::from_string("400: Bad Request (malformed json)");
                let response = response.with_status_code(400);
                let _ = request.respond(response);
                continue;
            }
        };

        let channel_info_result = get_channel_info(&req_struct.key, &config);
        let channel_info = match channel_info_result {
            Ok(message) => message,
            Err(_error) => {
                let response = Response::from_string("401: Unauthorized");
                let response = response.with_status_code(401);
                let _ = request.respond(response);
                continue;
            }
        };

        if !req_struct.url.starts_with("http") {
            let response = Response::from_string("400: Bad Request (no http on URL)");
            let response = response.with_status_code(400);
            let _ = request.respond(response);
            continue;
        }

        let discord_channel_id = serenity::model::id::ChannelId(channel_info.channel_id);

        discord::send_embed(
            discord_channel_id,
            &req_struct.title,
            &req_struct.url,
            &req_struct.body,
        );

        let response = Response::from_string("200: Message sent");
        let _ = request.respond(response);
    }
}

fn get_channel_info<'a>(
    channel_key: &str,
    config: &'a config::Config,
) -> std::result::Result<&'a config::Channel, String> {
    for channel in &config.channel {
        if channel.key == channel_key {
            return Ok(channel);
        }
    }
    Err("key not in config".to_owned())
}
