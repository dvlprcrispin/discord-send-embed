#![allow(unknown_lints)]
#![warn(clippy::all)]
#![warn(clippy::pedantic)]

use std::thread;

mod config;
mod discord;
mod web;

fn main() {
    // config stuff
    let config = config::make_config();

    // discord stuff
    let discord_client = discord::make_client(&config);

    //web stuff
    let web_server = web::make_server(&config);

    //start threads
    let discord_thread = thread::spawn(|| {
        discord::start_discord(discord_client);
    });

    let web_thread = thread::spawn(move || {
        web::start_web(&web_server, &config);
    });

    discord_thread.join().expect("error in discord thread");
    web_thread.join().expect("error in web thread");
}
